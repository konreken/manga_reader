#include <QApplication>
#include <iostream>
#include <QtWidgets>
#include <QString>
#include "mainwindow.h"

using namespace std;

ostream &operator<< (ostream &stream, QSize size){
	stream << "{width: " << size.width() << ", height: " << size.height() << "}";
	return stream;
}

int main(int argc, char* argv[]){
	QApplication app(argc, argv);
	MainWindow w;
	
	w.show();
	return app.exec();

	QWidget window;
	window.setWindowTitle(QString("MangaReader"));
	window.resize(720, 720);
	QLabel *label2 = new QLabel("Hello, world\n");
	label2->show();
	label2->setParent(&window);
	
	QPushButton button;

	
	QMenuBar menu(&window);
	menu.resize(10, 720);
	menu.show();
	menu.addMenu("File");
	menu.addAction("File");
	menu.addAction("Help");

	QPixmap pixmap("example.jpeg");
	if(pixmap.isNull()){
		cout << "wtf, man" << endl;
		return 1;
	}
	QLabel label(&window);
	cout << "Image Size: " << pixmap.size() << endl;
	label.setGeometry(0, 100, pixmap.width(), pixmap.height());
	label.setPixmap(pixmap);
	label.move(100,100);
	label.show();
	
	window.show();
	app.setActiveWindow(&window);
	cout << "Hello, world" << endl;
	return app.exec();
}