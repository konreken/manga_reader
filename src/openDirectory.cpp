#include "mainwindow.h"
#include <QFileDialog>

void MainWindow::open(){
    QFileDialog dialog;
    dialog.setFileMode(QFileDialog::Directory);
    //QAction *act = new QAction();

    QStringList list;
    if(dialog.exec()){
        list = dialog.selectedFiles();
        currentDirectory.setPath(list[0]);

        images = currentDirectory.entryList(QStringList() << "*.jpeg" << "*.JPG", QDir::Files);
        imageIterator = images.begin();
        updatePage();
        /*QSize max(0, 0);
        for(auto &item : images){

        }*/
    }

}
