#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <QGraphicsView>
#include <QLabel>
#include <QPixmap>
#include <QAction>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <QString>
#include <QKeyEvent>

using namespace std;

class NextImage : public QAction{
    QStringList::Iterator _iterator;
    QLabel *_label;

public:
    NextImage(QStringList::Iterator iterator, QLabel *label){
        _iterator = iterator;
        _label = label;
    }
};

void MainWindow::updatePage(){
    QPixmap pixmap(currentDirectory.absoluteFilePath(*imageIterator));
    QPixmap scaledPixmap = pixmap.scaled(ui->label->size(), Qt::KeepAspectRatio);
    ui->label->setPixmap(scaledPixmap);
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::open);
    //connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(open()));
    connect(ui->nextPage, &QPushButton::clicked, this, &MainWindow::nextPage);
    connect(ui->prevPage, &QPushButton::clicked, this, &MainWindow::prevPage);

    setWindowTitle("Manga Reader");
    currentDirectory.setPath("test_manga");

    images = currentDirectory.entryList(QStringList() << "*.jpeg" << "*.JPG", QDir::Files);
    imageIterator = images.begin();

    updatePage();
}

void MainWindow::keyPressEvent(QKeyEvent *event){
    if(event->type() == QEvent::KeyPress){
        switch(event->key()){
        case Qt::Key_Right:
            nextPage();
            break;
        case Qt::Key_Left:
            prevPage();
            break;
        }
    }
}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::nextPage()
{
    imageIterator++;
    if (imageIterator < images.end()){
        updatePage();
    } else{
        imageIterator = images.begin();
        updatePage();
    }
}


void MainWindow::prevPage()
{
    imageIterator--;
    if (imageIterator >= images.begin()){
        updatePage();
    } else{
        imageIterator = images.end() - 1;
        updatePage();
    }
}

