#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPixmap>
#include <QDir>

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

private:
    void keyPressEvent(QKeyEvent *event) override;

    void open();
    void nextPage();

    void prevPage();

    void updatePage();

    QStringList::Iterator imageIterator;
    QDir currentDirectory;

    QStringList images;

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
